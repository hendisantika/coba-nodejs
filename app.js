// use path module
const path = require('path');
// use express module
const express = require('express');
// use hbs template engine
const hbs = require('hbs');
// use body parse middleware
const bodyParser = require('body-parser');
const app = express();

// set dynamic views site
app.set('views', path.join(__dirname, 'views'));
// set view engine
app.set('view engine', 'hbs');

app.use(bodyParser.urlencoded({extended: false}));

// set public folder as static folder for static files
app.use(express.static('public'));

// route untuk halaman home
app.get('/', (req, res) => {
    res.render('index', {
        name: "Hendi Santika"
    })
});

// route untuk halaman home dengan parameter name
// app.get('/:name', (req, res) => {
//     res.render('index', {
//         name: req.params.name
//     });
// });


// route untuk menampilkan halaman form
app.get('/post', (req, res) => {
    res.render('form');
});

// route untuk submit form
app.post('/post', (req, res) => {
    res.render('index', {
        name: req.body.textname
    });
});

// route untuk halaman about
app.get('/about', (req, res) => {
    res.send('This is About Page')
});

app.listen(8000, () => {
    console.log('Server is running at port 8000');
});

