# Coba NodeJS

Untuk menjalankan project ini ketikan perintah berikut : 

`npm install` kemudian `node app.js` kemudian buka browser favoritmu. 
Lalu ketikan `http://localhost:8000` 


#### Screenshot
Home Page

![Home Page](img/home.png 'Home Page')

Form Page

![Form Page](img/post.png 'Form Page')

Post Page

![Post Page](img/post2.png 'Post Page')

Sumber artikel --> [klik di sini!](http://mfikri.com/artikel/tutorial-nodejs)